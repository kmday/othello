﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace OthelloMVC
{
    class AIPlayer:IOthelloPlayer
    {
        private int color;
        private int difficulty;
        private OthelloController controller;
        private OthelloModel model;
        private System.Drawing.Point maxMove;

        public AIPlayer(int c, int difficulty)
        {
            color = c;
            this.difficulty = difficulty;
        }

        public void makeMove(int x, int y)
        {
            // do nothing, AI players don't get input from the view
        }

        public void setController(OthelloController controller)
        {
            this.controller = controller;
            controller.setPlayer(this);
        }

        public void setModel(OthelloModel model)
        {
            this.model = model;
        }

        public int getColor()
        {
            return color;
        }

        public void yourTurn()
        {
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(minimaxDecision));
            thread.Start();
            while (!thread.IsAlive) ;
            thread.Join();
            controller.makeMove(maxMove.X, maxMove.Y, color);
        }

        private void minimaxDecision()
        {
            int max = -1;
            int alpha = -1;
            int beta = Globals.BOARD_SIZE + 1;
            List<System.Drawing.Point> legalMoves = controller.getLegalMoves(color);
            maxMove = legalMoves[0];
            GameState gameState = new GameState(model);
            foreach (System.Drawing.Point move in legalMoves)
            {
                int value = -1;
                GameState resultGameState = gameState.result(move.X, move.Y);
                int resultActivePlayer = resultGameState.getActivePlayer();
                if (resultActivePlayer == color)
                {
                    value = maxDecision(resultGameState, 0, alpha, beta);
                }
                else if (0 != resultActivePlayer)
                {
                    value = minDecision(resultGameState, 1, alpha, beta);
                }
                else
                {
                    value = resultGameState.value(color);
                }
                if (value > max)
                {
                    max = value;
                    maxMove = move;
                }
            }
        }

        private int minDecision(GameState gameState, int depth, int alpha, int beta)
        {
            int rval = 0;
            if (depth > difficulty)
            {
                rval = gameState.value(color);
            }
            else
            {
                int min = Globals.BOARD_SIZE + 1;
                foreach (System.Drawing.Point move in gameState.getLegalMoves())
                {
                    int value = Globals.BOARD_SIZE + 1;
                    GameState resultGameState = gameState.result(move.X, move.Y);
                    int resultActivePlayer = resultGameState.getActivePlayer();
                    if (resultActivePlayer == color)
                    {
                        value = maxDecision(resultGameState, depth + 1, alpha, beta);
                    }
                    else if (0 != resultActivePlayer)
                    {
                        value = minDecision(resultGameState, depth, alpha, beta);
                    }
                    else
                    {
                        value = resultGameState.value(color);
                    }
                    if (value < min)
                    {
                        min = value;
                    }
                    if (min <= alpha)
                    {
                        break;
                    }
                    else if(min < beta)
                    {
                        beta = min;
                    }
                }
                rval = min;
            }
            return rval;
        }

        private int maxDecision(GameState gameState, int depth, int alpha, int beta)
        {
            int rval = 0;
            if (depth > difficulty)
            {
                rval = gameState.value(color);
            }
            else
            {
                int max = -1;
                foreach (System.Drawing.Point move in gameState.getLegalMoves())
                {
                    int value = -1;
                    GameState resultGameState = gameState.result(move.X, move.Y);
                    int resultActivePlayer = resultGameState.getActivePlayer();
                    if (resultActivePlayer == color)
                    {
                        value = maxDecision(resultGameState, depth, alpha, beta);
                    }
                    else if (0 != resultActivePlayer)
                    {
                        value = minDecision(resultGameState, depth + 1, alpha, beta);
                    }
                    else
                    {
                        value = resultGameState.value(color);
                    }
                    if (value > max)
                    {
                        max = value;
                    }
                    if (max >= alpha)
                    {
                        break;
                    }
                    else if (max > alpha)
                    {
                        alpha = max;
                    }
                }
                rval = max;
            }
            return rval;
        }

    }
}
