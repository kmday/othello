﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OthelloMVC
{
    interface IOthelloObserver
    {
        void updateBoard(int [] theBoard);
        void updatePosition(int x, int y, int value);
        void updatePlayer(int player);
        void unsubscribe();
    }

    interface IOthelloObservable
    {
        IDisposable Subscribe(IOthelloObserver observer);
    }

    interface IOthelloPlayer
    {
        void makeMove(int x, int y);
        void setController(OthelloController controller);
        void setModel(OthelloModel model);
        int getColor();
        void yourTurn();
    }
}
