﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OthelloMVC
{
    class HumanPlayer: IOthelloPlayer
    {
        private int color;
        private OthelloController controller;

        public HumanPlayer(int c)
        {
            color = c;
            controller = null;
        }

        public void makeMove(int x, int y)
        {
            controller.makeMove(x, y, color);
        }

        public void setController(OthelloController controller)
        {
            this.controller = controller;
            controller.setPlayer(this);
        }

        public void setModel(OthelloModel model)
        {
            // do nothing, human players have the view to see the model
        }

        public int getColor()
        {
            return color;
        }

        public void yourTurn()
        {
            // ignore controller notification, human players make their moves through the view
        }
    }
}
