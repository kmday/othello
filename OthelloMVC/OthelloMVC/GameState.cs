﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OthelloMVC
{
    class GameState
    {
        private OthelloModel model;
        private OthelloController controller;

        public GameState(OthelloModel theModel)
        {
            model = new OthelloModel();
            controller = new OthelloController(model);
            model.setBoard(theModel.getBoard());
            model.setActivePlayer(theModel.getActivePlayer());
            IOthelloPlayer min = new HumanPlayer(Globals.BLACK_PLAYER);
            IOthelloPlayer max = new HumanPlayer(Globals.WHITE_PLAYER);
            min.setController(controller);
            max.setController(controller);
        }

        public GameState result(int x, int y)
        {
            GameState rval = new GameState(model);
            rval.controller.makeMove(x, y, model.getActivePlayer());
            return rval;
        }

        public int value(int player)
        {
            int[] scores = model.getScores();
            int rval = scores[player];
            if ((player == Globals.BLACK_PLAYER) && (0 == scores[Globals.WHITE_PLAYER]))
            {
                rval = Globals.BOARD_SIZE;
            }
            return rval;
        }

        public List<System.Drawing.Point> getLegalMoves()
        {
            return controller.getLegalMoves(model.getActivePlayer());
        }

        public int getActivePlayer()
        {
            return model.getActivePlayer();
        }
    }
}
