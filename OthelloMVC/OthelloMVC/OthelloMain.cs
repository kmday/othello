﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OthelloMVC
{
    public partial class OthelloMain : Form, IOthelloObserver
    {
        private IDisposable unsubscriber;
        private OthelloModel model;
        private OthelloController controller;
        private Globals.MakeMove makeMove;
        private IOthelloPlayer[] players;

        private Bitmap Blank;
        private Bitmap Black;
        private Bitmap White;

        private Graphics graphics;

        private const int SQUARE_SIZE = 34;

        public OthelloMain()
        {
            InitializeComponent();
            model = new OthelloModel();
            controller = new OthelloController(model);
            unsubscriber = model.Subscribe(this);

            System.Reflection.Assembly thisAssembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream imageStream = thisAssembly.GetManifestResourceStream("OthelloMVC.Blank.png");
            Blank = new Bitmap(imageStream);

            imageStream = thisAssembly.GetManifestResourceStream("OthelloMVC.Black.png");
            Black = new Bitmap(imageStream);

            imageStream = thisAssembly.GetManifestResourceStream("OthelloMVC.White.png");
            White = new Bitmap(imageStream);

            pboxBlack.Image = Black;
            pboxWhite.Image = White;
            lblBlackScore.Text = "0";
            lblWhiteScore.Text = "0";
            
            graphics = pboxBoard.CreateGraphics();

            makeMove = null;
            players = null;
        }

        private void mnuNewGame_Click(object sender, EventArgs e)
        {
            players = new IOthelloPlayer[Globals.MAX_VALUE + 1];
            if (humanToolStripMenuItem.Checked)
            {
                players[Globals.BLACK_PLAYER] = new HumanPlayer(Globals.BLACK_PLAYER);
                players[Globals.WHITE_PLAYER] = new HumanPlayer(Globals.WHITE_PLAYER);
            }
            else if (aIBlackToolStripMenuItem.Checked)
            {
                players[Globals.BLACK_PLAYER] = new AIPlayer(Globals.BLACK_PLAYER, int.Parse(blackDifficultyToolStripMenuItem.Text));
                players[Globals.WHITE_PLAYER] = new HumanPlayer(Globals.WHITE_PLAYER);
            }
            else if (aIWhiteToolStripMenuItem.Checked)
            {
                players[Globals.BLACK_PLAYER] = new HumanPlayer(Globals.BLACK_PLAYER);
                players[Globals.WHITE_PLAYER] = new AIPlayer(Globals.WHITE_PLAYER, int.Parse(whiteDifficultyToolStripMenuItem.Text));
            }

            players[Globals.BLACK_PLAYER].setController(controller);
            players[Globals.WHITE_PLAYER].setController(controller);
            players[Globals.BLACK_PLAYER].setModel(model);
            players[Globals.WHITE_PLAYER].setModel(model);

            makeMove = players[Globals.BLACK_PLAYER].makeMove;
            controller.newGame();
        }

        public void updateBoard(int[] theBoard)
        {
            displayBoard();
            displayScore();
        }

        public void updatePosition(int x, int y, int value)
        {
            displayBoard();
            displayScore();
        }

        public void updatePlayer(int player)
        {
            if (Globals.BLACK_PLAYER == player)
            {
                pboxCurrentPlayer.Image = Black;
                makeMove = players[Globals.BLACK_PLAYER].makeMove;
            }
            else if (Globals.WHITE_PLAYER == player)
            {
                pboxCurrentPlayer.Image = White;
                makeMove = players[Globals.WHITE_PLAYER].makeMove;
            }
            else
            {
                pboxCurrentPlayer.Image = Blank;
                makeMove = null;
            }
        }

        public void unsubscribe()
        {
            unsubscriber.Dispose();
        }

        private void displayBoard()
        {
            int[] board = model.getBoard();
            for (int i = 0; i < board.Length; ++i)
            {
                int x = (i % 8) * SQUARE_SIZE;
                int y = (i / 8) * SQUARE_SIZE;
                Rectangle rect = new Rectangle(x, y, SQUARE_SIZE, SQUARE_SIZE);
                Image image = Blank;
                if (1 == board[i])
                {
                    image = Black;
                }
                else if (2 == board[i])
                {
                    image = White;
                }
                graphics.DrawImage(image, rect);
            }
        }

        private void displayScore()
        {
            int[] scores = model.getScores();
            lblBlackScore.Text = scores[Globals.BLACK_PLAYER].ToString();
            lblWhiteScore.Text = scores[Globals.WHITE_PLAYER].ToString();
        }

        private void pboxBoard_MouseClick(object sender, MouseEventArgs e)
        {
            int x = e.X / SQUARE_SIZE;
            int y = e.Y / SQUARE_SIZE;
            if (null != makeMove)
            {
                makeMove(x, y);
            }
        }

        private void setOpponentCheckedState(object sender)
        {
            foreach (ToolStripMenuItem tsb in ((ToolStripMenuItem)sender).GetCurrentParent().Items)
            {
                tsb.Checked = (tsb == sender);
            }
        }

        private void humanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setOpponentCheckedState(sender);
        }

        private void aIBlackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setOpponentCheckedState(sender);
        }

        private void aIWhiteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            setOpponentCheckedState(sender);
        }

        private void demoAIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            players = new IOthelloPlayer[Globals.MAX_VALUE + 1];
            players[Globals.BLACK_PLAYER] = new AIPlayer(Globals.BLACK_PLAYER, 7);
            players[Globals.WHITE_PLAYER] = new AIPlayer(Globals.WHITE_PLAYER, 7);

            players[Globals.BLACK_PLAYER].setController(controller);
            players[Globals.WHITE_PLAYER].setController(controller);
            players[Globals.BLACK_PLAYER].setModel(model);
            players[Globals.WHITE_PLAYER].setModel(model);

            makeMove = players[Globals.BLACK_PLAYER].makeMove;
            controller.newGame();
        }

    }
}
