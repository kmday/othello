﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OthelloMVC
{
    class OthelloController
    {
        private const int UNKNOWN = -1;
        private IOthelloPlayer[] players;

        private delegate bool directionalOperation(int x, int y, int xDir, int yDir, int player);

        private OthelloModel model;

        public OthelloController(OthelloModel theModel)
        {
            model = theModel;
            players = new IOthelloPlayer[Globals.MAX_VALUE + 1];
        }

        public bool newGame()
        {
            bool rval = true;

            int[] newGameBoard = new int[Globals.BOARD_SIZE];
            for (int i = 0; i < newGameBoard.Length; ++i)
            {
                newGameBoard[i] = Globals.EMPTY;
            }
            newGameBoard[27] = Globals.WHITE_PLAYER;
            newGameBoard[28] = Globals.BLACK_PLAYER;
            newGameBoard[35] = Globals.BLACK_PLAYER;
            newGameBoard[36] = Globals.WHITE_PLAYER;

            rval = model.setBoard(newGameBoard) && model.setActivePlayer(Globals.BLACK_PLAYER);

            players[Globals.BLACK_PLAYER].yourTurn();

            return rval;
        }

        public void setPlayer(IOthelloPlayer c)
        {
            players[c.getColor()] = c;
        }

        public bool makeMove(int x, int y, int player)
        {
            bool rval = (player == model.getActivePlayer()) &&
                   getLegalMoves(player).Contains(new System.Drawing.Point(x, y));

            rval = rval && model.setPosition(x, y, player) && flipFlanked(x, y);

            if (rval)
            {
                int newPlayer = (model.getActivePlayer() == Globals.WHITE_PLAYER) ? Globals.BLACK_PLAYER : Globals.WHITE_PLAYER;
                if (getLegalMoves(newPlayer).Count != 0)
                {
                    model.setActivePlayer(newPlayer);
                    players[newPlayer].yourTurn();
                }
                else if (getLegalMoves(player).Count == 0)
                {
                    model.setActivePlayer(Globals.EMPTY);
                    // end of game
                }
                else
                {                    
                    players[model.getActivePlayer()].yourTurn();
                }
            }

            return rval;
        }

        public List<System.Drawing.Point> getLegalMoves(int player)
        {
            List<System.Drawing.Point> rval = new List<System.Drawing.Point>();

            for (int y = 0; y < Globals.BOARD_DIMENSION; ++y)
            {
                for (int x = 0; x < Globals.BOARD_DIMENSION; ++x)
                {
                    if (isEmpty(x, y) && hasFlanking(x, y, player))
                    {
                        rval.Add(new System.Drawing.Point(x, y));
                    }
                }
            }

            return rval;
        }

        private bool hasFlanking(int x, int y, int xDir, int yDir, int player)
        {
            int distance = 1;
            int xVal = x + (xDir * distance);
            int yVal = y + (yDir * distance);

            while (isOpposing(xVal, yVal, player))
            {
                ++distance;
                xVal = x + (xDir * distance);
                yVal = y + (yDir * distance);
            }

            return ((distance > 1) && isFriendly(xVal, yVal, player));
        }

        private bool hasFlanking(int x, int y, int player)
        {
            bool rval = false;

            for (int yDir = -1; (!rval) && (yDir <= 1); ++yDir)
            {
                for (int xDir = -1; (!rval) && (xDir <= 1); ++xDir)
                {
                    if ((xDir != 0) || (yDir != 0))
                    {
                        rval = hasFlanking(x, y, xDir, yDir, player);
                    }
                }
            }
            return rval;
        }

        private bool flipFlanked(int x, int y)
        {
            Dictionary<System.Drawing.Point, int> flips = new Dictionary<System.Drawing.Point,int>();

            for (int yDir = -1; (yDir <= 1); ++yDir)
            {
                for (int xDir = -1; (xDir <= 1); ++xDir)
                {
                    if ((xDir != 0) || (yDir != 0))
                    {
                        int dist = flankingDistance(x, y, xDir, yDir, model.getActivePlayer());
                        if (0 != dist)
                        {
                            flips.Add(new System.Drawing.Point(xDir, yDir), dist);
                        }
                    }
                }
            }

            foreach (KeyValuePair<System.Drawing.Point, int> kvp in flips)
            {
                doFlips(x, y, kvp.Key.X, kvp.Key.Y, kvp.Value, model.getActivePlayer());
            }

            return (flips.Count > 0);
        }

        private int flankingDistance(int x, int y, int xDir, int yDir, int player)
        {
            int rval = 0;
            int distance = 1;
            int xVal = x + (xDir * distance);
            int yVal = y + (yDir * distance);

            while (isOpposing(xVal, yVal, player))
            {
                ++distance;
                xVal = x + (xDir * distance);
                yVal = y + (yDir * distance);
            }

            if ((distance > 1) && isFriendly(xVal, yVal, player))
            {
                rval = distance;
            }

            return rval;
        }

        private void doFlips(int x, int y, int xDir, int yDir, int distance, int player)
        {
            while (0 < distance)
            {
                int xVal = x + (xDir * distance);
                int yVal = y + (yDir * distance);
                model.setPosition(xVal, yVal, player);
                --distance;
            }
        }

        private bool isEmpty(int x, int y)
        {
            int positionValue = UNKNOWN;
            return (model.getPosition(x, y, out positionValue) &&
                    (Globals.EMPTY == positionValue));
        }

        private bool isFriendly(int x, int y, int player)
        {
            int positionValue = UNKNOWN;
            return (model.getPosition(x, y, out positionValue) &&
                    (player == positionValue));
        }

        private bool isOpposing(int x, int y, int player)
        {
            int positionValue = UNKNOWN;
            return (model.getPosition(x, y, out positionValue) &&
                    (player != positionValue) &&
                    (Globals.EMPTY != positionValue));
        }
    }
}
