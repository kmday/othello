﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OthelloMVC
{
    static class Globals
    {
        public const int EMPTY = 0;
        public const int BLACK_PLAYER = 1;
        public const int WHITE_PLAYER = 2;
        public const int MAX_VALUE = 2;
        public const int BOARD_DIMENSION = 8;
        public const int MAX_DIMENSION_INDEX = BOARD_DIMENSION - 1;
        public const int BOARD_SIZE = BOARD_DIMENSION * BOARD_DIMENSION;
        public delegate void MakeMove(int x, int y);
    }
}
