﻿namespace OthelloMVC
{
    partial class OthelloMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNewGame = new System.Windows.Forms.ToolStripMenuItem();
            this.opponentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.humanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aIBlackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aIWhiteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pboxBoard = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pboxCurrentPlayer = new System.Windows.Forms.PictureBox();
            this.pboxBlack = new System.Windows.Forms.PictureBox();
            this.pboxWhite = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblBlackScore = new System.Windows.Forms.Label();
            this.lblWhiteScore = new System.Windows.Forms.Label();
            this.blackDifficultyToolStripMenuItem = new System.Windows.Forms.ToolStripTextBox();
            this.whiteDifficultyToolStripMenuItem = new System.Windows.Forms.ToolStripTextBox();
            this.demoAIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBoard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCurrentPlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBlack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxWhite)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem,
            this.opponentToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(622, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuNewGame,
            this.demoAIToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // mnuNewGame
            // 
            this.mnuNewGame.Name = "mnuNewGame";
            this.mnuNewGame.Size = new System.Drawing.Size(98, 22);
            this.mnuNewGame.Text = "New";
            this.mnuNewGame.Click += new System.EventHandler(this.mnuNewGame_Click);
            // 
            // opponentToolStripMenuItem
            // 
            this.opponentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.humanToolStripMenuItem,
            this.aIBlackToolStripMenuItem,
            this.aIWhiteToolStripMenuItem});
            this.opponentToolStripMenuItem.Name = "opponentToolStripMenuItem";
            this.opponentToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.opponentToolStripMenuItem.Text = "Opponent";
            // 
            // humanToolStripMenuItem
            // 
            this.humanToolStripMenuItem.Checked = true;
            this.humanToolStripMenuItem.CheckOnClick = true;
            this.humanToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.humanToolStripMenuItem.Name = "humanToolStripMenuItem";
            this.humanToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.humanToolStripMenuItem.Text = "Human";
            this.humanToolStripMenuItem.Click += new System.EventHandler(this.humanToolStripMenuItem_Click);
            // 
            // aIBlackToolStripMenuItem
            // 
            this.aIBlackToolStripMenuItem.CheckOnClick = true;
            this.aIBlackToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blackDifficultyToolStripMenuItem});
            this.aIBlackToolStripMenuItem.Name = "aIBlackToolStripMenuItem";
            this.aIBlackToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aIBlackToolStripMenuItem.Text = "AI - Black";
            this.aIBlackToolStripMenuItem.Click += new System.EventHandler(this.aIBlackToolStripMenuItem_Click);
            // 
            // aIWhiteToolStripMenuItem
            // 
            this.aIWhiteToolStripMenuItem.CheckOnClick = true;
            this.aIWhiteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.whiteDifficultyToolStripMenuItem});
            this.aIWhiteToolStripMenuItem.Name = "aIWhiteToolStripMenuItem";
            this.aIWhiteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aIWhiteToolStripMenuItem.Text = "AI - White";
            this.aIWhiteToolStripMenuItem.Click += new System.EventHandler(this.aIWhiteToolStripMenuItem_Click);
            // 
            // pboxBoard
            // 
            this.pboxBoard.Location = new System.Drawing.Point(0, 27);
            this.pboxBoard.Name = "pboxBoard";
            this.pboxBoard.Size = new System.Drawing.Size(272, 272);
            this.pboxBoard.TabIndex = 3;
            this.pboxBoard.TabStop = false;
            this.pboxBoard.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pboxBoard_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(278, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Current Player:";
            // 
            // pboxCurrentPlayer
            // 
            this.pboxCurrentPlayer.Location = new System.Drawing.Point(360, 27);
            this.pboxCurrentPlayer.Name = "pboxCurrentPlayer";
            this.pboxCurrentPlayer.Size = new System.Drawing.Size(34, 34);
            this.pboxCurrentPlayer.TabIndex = 5;
            this.pboxCurrentPlayer.TabStop = false;
            // 
            // pboxBlack
            // 
            this.pboxBlack.Location = new System.Drawing.Point(281, 185);
            this.pboxBlack.Name = "pboxBlack";
            this.pboxBlack.Size = new System.Drawing.Size(34, 34);
            this.pboxBlack.TabIndex = 6;
            this.pboxBlack.TabStop = false;
            // 
            // pboxWhite
            // 
            this.pboxWhite.Location = new System.Drawing.Point(360, 185);
            this.pboxWhite.Name = "pboxWhite";
            this.pboxWhite.Size = new System.Drawing.Size(34, 34);
            this.pboxWhite.TabIndex = 7;
            this.pboxWhite.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(280, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Score:";
            // 
            // lblBlackScore
            // 
            this.lblBlackScore.AutoSize = true;
            this.lblBlackScore.Location = new System.Drawing.Point(289, 222);
            this.lblBlackScore.Name = "lblBlackScore";
            this.lblBlackScore.Size = new System.Drawing.Size(19, 13);
            this.lblBlackScore.TabIndex = 9;
            this.lblBlackScore.Text = "64";
            // 
            // lblWhiteScore
            // 
            this.lblWhiteScore.AutoSize = true;
            this.lblWhiteScore.Location = new System.Drawing.Point(368, 222);
            this.lblWhiteScore.Name = "lblWhiteScore";
            this.lblWhiteScore.Size = new System.Drawing.Size(19, 13);
            this.lblWhiteScore.TabIndex = 10;
            this.lblWhiteScore.Text = "64";
            // 
            // blackDifficultyToolStripMenuItem
            // 
            this.blackDifficultyToolStripMenuItem.Name = "blackDifficultyToolStripMenuItem";
            this.blackDifficultyToolStripMenuItem.Size = new System.Drawing.Size(152, 23);
            this.blackDifficultyToolStripMenuItem.Text = "Difficulty";
            // 
            // whiteDifficultyToolStripMenuItem
            // 
            this.whiteDifficultyToolStripMenuItem.Name = "whiteDifficultyToolStripMenuItem";
            this.whiteDifficultyToolStripMenuItem.Size = new System.Drawing.Size(152, 23);
            this.whiteDifficultyToolStripMenuItem.Text = "Difficulty";
            // 
            // demoAIToolStripMenuItem
            // 
            this.demoAIToolStripMenuItem.Name = "demoAIToolStripMenuItem";
            this.demoAIToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.demoAIToolStripMenuItem.Text = "Demo AI";
            this.demoAIToolStripMenuItem.Click += new System.EventHandler(this.demoAIToolStripMenuItem_Click);
            // 
            // OthelloMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 441);
            this.Controls.Add(this.lblWhiteScore);
            this.Controls.Add(this.lblBlackScore);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pboxWhite);
            this.Controls.Add(this.pboxBlack);
            this.Controls.Add(this.pboxCurrentPlayer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pboxBoard);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "OthelloMain";
            this.Text = "Othello";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBoard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCurrentPlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxBlack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxWhite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuNewGame;
        private System.Windows.Forms.PictureBox pboxBoard;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pboxCurrentPlayer;
        private System.Windows.Forms.PictureBox pboxBlack;
        private System.Windows.Forms.PictureBox pboxWhite;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblBlackScore;
        private System.Windows.Forms.Label lblWhiteScore;
        private System.Windows.Forms.ToolStripMenuItem opponentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem humanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aIBlackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aIWhiteToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox blackDifficultyToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox whiteDifficultyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem demoAIToolStripMenuItem;
    }
}

