﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OthelloMVC
{

    class OthelloModel:IOthelloObservable
    {
        #region IOthelloObservable

        private List<IOthelloObserver> observers = new List<IOthelloObserver>();

        private class Unsubscriber : IDisposable
        {
            private List<IOthelloObserver> observers;
            private IOthelloObserver observer;

            public Unsubscriber(List<IOthelloObserver> theObservers, IOthelloObserver theObserver)
            {
                observers = theObservers;
                observer = theObserver;
            }

            public void Dispose()
            {
                if ((observer != null) && (observers.Contains(observer)))
                {
                    observers.Remove(observer);
                }
            }
        }

        public IDisposable Subscribe(IOthelloObserver observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
            return new Unsubscriber(observers, observer);
        }

        #endregion

        #region Private observer notification functions

        private void notifyObserversBoardChange()
        {
            if (isValidBoard(board))
            {
                foreach (var observer in observers)
                {
                    observer.updateBoard((int[])board.Clone());
                }
            }
        }

        private void notifyObserversPositionChange(int x, int y)
        {
            int index = getIndex(x, y);
            if (-1 != index)
            {
                foreach (var observer in observers)
                {
                    observer.updatePosition(x, y, board[index]);
                }
            }
        }

        private void notifyObserversPlayerChange(int player)
        {
            if (isValidValue(player))
            {
                foreach (var observer in observers)
                {
                    observer.updatePlayer(player);
                }
            }
        }

        #endregion

        #region Private instance variables



        private int[] board = new int[Globals.BOARD_SIZE];
        private int activePlayer;
        private int[] scores = new int[Globals.MAX_VALUE + 1];

        #endregion

        #region Private utility functions

        private static void initArray(int [] array, int value)
        {
            for (int i = 0; i < array.Length; ++i)
            {
                array[i] = value;
            }
        }

        private bool boundsCheck(int x, int y)
        {
            return ((x >= 0) && (x <= Globals.MAX_DIMENSION_INDEX) && (y >= 0) && (y <= Globals.MAX_DIMENSION_INDEX));
        }

        private bool isValidValue(int value)
        {
            return ((value >= 0) && (value <= Globals.MAX_VALUE));
        }

        private int getIndex(int x, int y)
        {
            int rval = -1;
            if(boundsCheck(x,y))
            {
                rval = y * Globals.BOARD_DIMENSION + x;
            }
            return rval;
        }

        private bool isValidBoard(int[] theBoard)
        {
            // make sure it's not null and they're the same size first
            bool rval = (theBoard != null) && (theBoard.Length == board.Length);

            // validate the data
            for (int i = 0; rval && (i < theBoard.Length); ++i)
            {
                rval = rval && isValidValue(theBoard[i]);
            }

            return rval;
        }

        private bool isDifferentBoard(int[] theBoard)
        {
            bool rval = true;

            // check the data
            for (int i = 0; rval && (i < theBoard.Length); ++i)
            {
                rval = rval && (theBoard[i] == board[i]);
            }

            rval = !rval;

            return rval;
        }

        #endregion

        public OthelloModel()
        {
            initArray(board, 0);
            activePlayer = 0;
            scores[Globals.WHITE_PLAYER] = 0;
            scores[Globals.BLACK_PLAYER] = 0;
            scores[Globals.EMPTY] = Globals.BOARD_SIZE;
        }

        public bool getPosition(int x, int y, out int value)
        {
            int index = getIndex(x, y);
            bool rval = (-1 != index);

            value = ((rval) ? (board[index]) : (-1));

            return rval;
        }

        public int [] getBoard()
        {
            // create a new array and copy the values in
            // don't pass a reference to our private array back
            int[] rval = new int[Globals.BOARD_SIZE];
            board.CopyTo(rval, 0);
            return rval;
        }

        public int getActivePlayer()
        {
            return activePlayer;
        }

        public bool setPosition(int x, int y, int value)
        {
            int index = getIndex(x, y);
            bool rval = (-1 != index) && isValidValue(value);

            if ((rval) && (value != board[index]))
            {
                ++scores[value];
                --scores[board[index]];
                board[index] = value;
                notifyObserversPositionChange(x, y);
            }

            return rval;
        }

        public bool setBoard(int[] newBoard)
        {
            bool rval = isValidBoard(newBoard);

            if (rval  && isDifferentBoard(newBoard))
            {
                // copy the new board values
                newBoard.CopyTo(board, 0);
                for (int i = 0; i <= Globals.MAX_VALUE; ++i)
                {
                    scores[i] = 0;
                }
                for (int i = 0; i < board.Length; ++i)
                {
                    ++scores[board[i]];
                }
                notifyObserversBoardChange();
            }

            return rval;
        }

        public bool setActivePlayer(int value)
        {
            bool rval = isValidValue(value);

            if ((rval) && (value != activePlayer))
            {
                activePlayer = value;
                notifyObserversPlayerChange(activePlayer);
            }

            return rval;
        }

        public int[] getScores()
        {
            // create a new array and copy the values in
            // don't pass a reference to our private array back
            int[] rval = new int[Globals.MAX_VALUE + 1];
            scores.CopyTo(rval, 0);
            return rval;
        }
    }
}
